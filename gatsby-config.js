module.exports = {
  siteMetadata: {
    title: `rodegherShowcase`,
    autor: `Vanessa Rodegher`
  },
    plugins: [
      `gatsby-transformer-sharp`, `gatsby-plugin-sharp`,
      {
        resolve: `gatsby-source-filesystem`,
        options: {
          name: `src`,
          path: `${__dirname}/src/`,
        },
      },
      `gatsby-transformer-remark`,
      {
        resolve: `gatsby-plugin-typography`,
        options: {
          pathToConfigModule: `src/utils/typography`,
        },
      },
    ],
  }
