import React from "react"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import Layout from "../components/layout"

export default ({ data }) => {
    const post = data.markdownRemark 
    return (
    <Layout>
      <h1>{post.frontmatter.title}</h1>
      <div dangerouslySetInnerHTML={{__html: post.html }}>
      </div>
      <div>
    <h1>Hello gatsby-image</h1>
    <Img
      fluid={post.frontmatter.image}
      alt="Gatsby Docs are awesome"
    />
  </div>
    </Layout>
  )
}

export const query = graphql`
    query($slug: String!) {
        markdownRemark(fields: { slug: { eq: $slug } }) {
            html
            frontmatter {
                title
                image 
          }
        }
      }
    
`