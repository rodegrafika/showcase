---
title: "The Pig Game"
date: "2019-04-03"
attachments: "./data/pig_game.jpg"
---

JavaScript version of the popular dice game with custom score setting function.


<a href="https://example.com" target="_blank" >live</a>

<a href="https://example.com"  target="_blank" >code</a>
