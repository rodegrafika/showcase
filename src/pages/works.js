import React from "react"
import { graphql, Link } from "gatsby"
import Layout from "../components/layout"



export default ({data}) => {
  console.log(data)
  return (
    <Layout>
    <div>
    <h1>Works</h1>
    {data.allMarkdownRemark.edges.map(({ node }) => (
          <div key={node.id}>
          <Link to={node.fields.slug}>
          <img src={node.frontmatter.attachments} alt="screenshot"/>
            <h3>
              {node.frontmatter.title}
            </h3>
            <p>{node.excerpt}</p>
            </Link>
          </div>
        ))}
      </div>
  </Layout>
  )
}
  
export const query = graphql`
  query {
    allMarkdownRemark (sort: { fields: [frontmatter___date], order: DESC }) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            attachments
          }
          fields {
            slug
          }
          excerpt
        }
      }
    }
  }
`