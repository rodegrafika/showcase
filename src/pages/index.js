import React from "react"
import styles from "./index.module.css"
import { Link } from "gatsby"

export default () => (
<div className={styles.coverpage}>
    <div className={styles.container}>
        <div className={styles.title}>
            <h1>vanessa rodegher</h1>
            <h3>Frontend Developer/Designer</h3>
            <Link to="/contact/">Contact</Link>
        </div>
    </div>
</div>  

)